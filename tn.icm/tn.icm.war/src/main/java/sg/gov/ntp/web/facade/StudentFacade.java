package sg.gov.ntp.web.facade;

import java.math.BigDecimal;

import sg.gov.ntp.vo.Student;

public interface StudentFacade {
  public void saveStudent(Student student);
  public Student getStudent(BigDecimal id);
}
