package sg.gov.ntp.web.facade;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import sg.gov.ntp.business.service.StudentService;
import sg.gov.ntp.vo.Student;

@Component
public class StudentFacadeImpl implements StudentFacade {

  @Autowired
  private StudentService studentService;

  @Override
  public void saveStudent(Student student) {
    studentService.create(student);
  }

  @Override
  public Student getStudent(BigDecimal id) {
    return studentService.findById(id);
  }
}
