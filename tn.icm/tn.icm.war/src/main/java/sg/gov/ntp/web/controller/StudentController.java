package sg.gov.ntp.web.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sg.gov.ntp.common.controller.NTPBaseController;
import sg.gov.ntp.vo.Student;
import sg.gov.ntp.web.facade.StudentFacade;

@Controller
@RequestMapping(value = "/ntp")
public class StudentController extends NTPBaseController{

  @Autowired
  private StudentFacade studentFacade;

  @RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
  @ResponseBody
  public ResponseEntity<Student> saveStudent(@RequestBody Student student, BindingResult result) {

      studentFacade.saveStudent(student);

    return new ResponseEntity<>(student, HttpStatus.OK);
  }

  @RequestMapping(value = "/student/{id}", method = RequestMethod.GET, produces = "application/json", consumes = "application/json")
  @ResponseBody
  public ResponseEntity<Student> getStudent(@PathVariable("id") BigDecimal id) {
    Student student = studentFacade.getStudent(id);
    return new ResponseEntity<Student>(student, HttpStatus.OK);
  }
}