package tn.icm.war.facade;

import org.springframework.beans.factory.annotation.Autowired;

import sg.gov.ntp.business.service.StudentService;
import sg.gov.ntp.vo.Student;

public class StudentFacadeImpl implements StudentFacade {

  @Autowired
  private StudentService studentService;

  @Override
  public void saveStudent(Student student) {
    // TODO Auto-generated method stub
    //studentService.saveStudent(student);
    studentService.save(student);
  }

  @Override
  public Student getStudent(String id) {
    // TODO Auto-generated method stub
    return null;
  }

}
