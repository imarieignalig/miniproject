package tn.icm.war.facade;

import sg.gov.ntp.vo.Student;

public interface StudentFacade {
  public void saveStudent(Student student);
  public Student getStudent(String id);
}
