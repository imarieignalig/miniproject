package tn.icm.war.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sg.gov.ntp.vo.Student;
import tn.icm.war.facade.StudentFacade;

@Controller
@RequestMapping(value="/ntp")
public class StudentController {

  private StudentFacade studentFacade;

  @RequestMapping(value="/save", method=RequestMethod.POST, produces="application/json", consumes="application/json")
  @ResponseBody
  public ResponseEntity<String> saveStudent(@RequestBody Student student, BindingResult result){
    ResponseEntity<String> entity;

    if(result.hasErrors()) {
      entity = new ResponseEntity<>("failed", HttpStatus.NOT_ACCEPTABLE);
    }else {
      studentFacade.saveStudent(student);
      entity = new ResponseEntity<>("success", HttpStatus.ACCEPTED);
    }
    return entity;
  }

  @RequestMapping(value="/ntp/student/{id}", method=RequestMethod.GET, produces="application/json", consumes="application/json")
  @ResponseBody
  public ResponseEntity<String> getStudent(@PathVariable String input){
    return new ResponseEntity<String>(input, HttpStatus.OK);

  }
}
