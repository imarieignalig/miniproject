package sg.gov.ntp.web.facade;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import sg.gov.ntp.business.service.StudentService;
import sg.gov.ntp.vo.Student;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class StudentFacadeImplTest {

  @Mock
  StudentService studentService;

  @InjectMocks
  StudentFacadeImpl studentFacadeImpl;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void saveStudent() throws Exception {

    Student student = new Student();
    student.setStudentId(new BigDecimal(1));
    student.setFname("Imarie");
    student.setLname("Ignalig");
    student.setAge(new BigDecimal(23));
    student.setGender("m");
    studentFacadeImpl.saveStudent(student);

    Mockito.when(studentService.create(Mockito.any(Student.class)))
        .thenReturn(student);

    studentFacadeImpl.saveStudent(Mockito.any(Student.class));
    Mockito.verify(studentService).create(Mockito.any(Student.class));
  }

  @Test
  public void getStudent() throws Exception {

    Student student = new Student();
    student.setStudentId(new BigDecimal(1));
    student.setFname("Imarie");
    student.setLname("Ignalig");
    student.setAge(new BigDecimal(21));
    student.setGender("f");
    studentFacadeImpl.getStudent(new BigDecimal(1));

    Mockito.when(studentService.findById(Mockito.any(BigDecimal.class)))
        .thenReturn(student);

    studentFacadeImpl.getStudent(Mockito.any(BigDecimal.class));
    Mockito.verify(studentService).findById(Mockito.any(BigDecimal.class));
  }
}
