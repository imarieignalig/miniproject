package sg.gov.ntp.web.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import net.minidev.json.JSONObject;
import sg.gov.ntp.business.service.StudentService;
import sg.gov.ntp.vo.Student;
import sg.gov.ntp.web.facade.StudentFacade;
import sg.gov.ntp.web.facade.StudentFacadeImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class StudentControllerTest {

  @Mock
  StudentFacadeImpl studenFacadeImpl;

  @Mock
  StudentFacade studentFacade;

  @Mock
  StudentService studentService;

  @InjectMocks
  StudentController studentController;

  private MockMvc mockMvc;

  @Before
  public void setup() throws Exception {
    MockitoAnnotations.initMocks(this);
    this.mockMvc = MockMvcBuilders.standaloneSetup(studentController).build();
  }

  @Test
  public void saveStudent() throws Exception {

    Mockito.when(studentService.create(Mockito.any(Student.class)))
        .thenReturn(new Student());

    JSONObject jsonObject = new JSONObject();
    jsonObject.put("gender", "f");
    jsonObject.put("studentId", 7);
    jsonObject.put("fname", "Imarie");
    jsonObject.put("lname", "Ignalig");
    jsonObject.put("age", 21);
    jsonObject.put("gender", "f");

    System.out.println(jsonObject.toString());
    MvcResult result = this.mockMvc
        .perform(post("/ntp/save").content(jsonObject.toJSONString())
            .header("transactionId", "1").header("sessionId", "1")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON))
        .andDo(print()).andExpect(status().isOk()).andReturn();

    assertEquals(result.getResponse().getStatus(), HttpStatus.OK.value());
    Mockito.verify(studentFacade).saveStudent(Mockito.any(Student.class));
  }

  @Test
  public void getStudent() throws Exception {

    Mockito.when(studentFacade.getStudent(Mockito.any(BigDecimal.class)))
        .thenReturn(new Student());

    JSONObject jsonObject = new JSONObject();
    jsonObject.put("gender", "f");
    jsonObject.put("studentId", 2);
    jsonObject.put("fname", "Imarie");
    jsonObject.put("lname", "Ignalig");
    jsonObject.put("age", 21);
    jsonObject.put("gender", "f");

    MvcResult result = this.mockMvc
        .perform(get("/ntp/student/2").content(jsonObject.toJSONString())
            .header("transactionId", "1").header("sessionId", "1")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON))
        .andDo(print()).andExpect(status().isOk()).andReturn();

    assertEquals(result.getResponse().getStatus(), HttpStatus.OK.value());
    Mockito.verify(studentFacade).getStudent(Mockito.any(BigDecimal.class));
  }
}
