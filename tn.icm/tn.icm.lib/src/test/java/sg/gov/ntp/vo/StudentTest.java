/*
 * Created on 7 Sep 2017 ( Time 14:00:01 )
 */
package sg.gov.ntp.vo;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import sg.gov.ntp.test.MockValues;


import java.math.BigDecimal;

public class StudentTest {

    private static Student studentTestValue = null;

    private static MockValues mockValues = new MockValues();

    private static BigDecimal studentIdInternalMockValue = mockValues.nextBigDecimal();
    private static String fnameInternalMockValue = mockValues.nextString(50);
    private static String lnameInternalMockValue = mockValues.nextString(50);
    private static BigDecimal ageInternalMockValue = mockValues.nextBigDecimal();
    private static String genderInternalMockValue = mockValues.nextString(2);

    @Before
    public void setUp() throws Exception {

    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        studentTestValue = new Student();
        
        // Prepare test data for Getter methods.
        studentTestValue.setStudentId(studentIdInternalMockValue);
        studentTestValue.setFname(fnameInternalMockValue);
        studentTestValue.setLname(lnameInternalMockValue);
        studentTestValue.setAge(ageInternalMockValue);
        studentTestValue.setGender(genderInternalMockValue);
    }

    @After
    public void tearDown() throws Exception {
    
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        studentTestValue = null;
        mockValues = null;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    @Test
    public void testGetStudentId () {
        assertEquals(studentIdInternalMockValue, studentTestValue.getStudentId());
    }

    @Test
    public void testSetStudentId () {
        studentTestValue.setStudentId(studentIdInternalMockValue);
        assertEquals(studentIdInternalMockValue, studentTestValue.getStudentId());
    }

    @Test
    public void testGetFname () {
        assertEquals(fnameInternalMockValue, studentTestValue.getFname());
    }

    @Test
    public void testSetFname () {
        studentTestValue.setFname(fnameInternalMockValue);
        assertEquals(fnameInternalMockValue, studentTestValue.getFname());
    }

    @Test
    public void testGetLname () {
        assertEquals(lnameInternalMockValue, studentTestValue.getLname());
    }

    @Test
    public void testSetLname () {
        studentTestValue.setLname(lnameInternalMockValue);
        assertEquals(lnameInternalMockValue, studentTestValue.getLname());
    }

    @Test
    public void testGetAge () {
        assertEquals(ageInternalMockValue, studentTestValue.getAge());
    }

    @Test
    public void testSetAge () {
        studentTestValue.setAge(ageInternalMockValue);
        assertEquals(ageInternalMockValue, studentTestValue.getAge());
    }

    @Test
    public void testGetGender () {
        assertEquals(genderInternalMockValue, studentTestValue.getGender());
    }

    @Test
    public void testSetGender () {
        studentTestValue.setGender(genderInternalMockValue);
        assertEquals(genderInternalMockValue, studentTestValue.getGender());
    }


}
