package sg.gov.ntp.test;

import sg.gov.ntp.vo.Classroom;
import java.math.BigDecimal;

import org.junit.Test;

public class ClassroomFactoryForTest {

  private MockValues mockValues = new MockValues();

  public Classroom newClassroom() {

    BigDecimal classroomId = mockValues.nextBigDecimal();

    Classroom classroom = new Classroom();
    classroom.setClassroomId(classroomId);
    return classroom;
  }

  @Test
  public void ClassroomFactoryForTest() {

  }
}
