/*
 * Created on 7 Sep 2017 ( Time 14:00:01 )
 */
package sg.gov.ntp.business.service.mapping;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

import sg.gov.ntp.vo.Student;
import sg.gov.ntp.vo.jpa.StudentEntity;
import sg.gov.ntp.test.MockValues;

/**
 * Test : Mapping between entity beans and display beans.
 */
public class StudentServiceMapperTest {

	private StudentServiceMapper studentServiceMapper;

	private static ModelMapper modelMapper = new ModelMapper();

	private MockValues mockValues = new MockValues();


	@BeforeClass
	public static void setUp() {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	@Before
	public void before() {
		studentServiceMapper = new StudentServiceMapper();
		studentServiceMapper.setModelMapper(modelMapper);
	}

	/**
	 * Mapping from 'StudentEntity' to 'Student'
	 * @param studentEntity
	 */
	@Test
	public void testMapStudentEntityToStudent() {
		// Given
		StudentEntity studentEntity = new StudentEntity();
		studentEntity.setFname(mockValues.nextString(50));
		studentEntity.setLname(mockValues.nextString(50));
		studentEntity.setAge(mockValues.nextBigDecimal());
		studentEntity.setGender(mockValues.nextString(2));

		// When
		Student student = studentServiceMapper.mapStudentEntityToStudent(studentEntity);

		// Then
		assertEquals(studentEntity.getFname(), student.getFname());
		assertEquals(studentEntity.getLname(), student.getLname());
		assertEquals(studentEntity.getAge(), student.getAge());
		assertEquals(studentEntity.getGender(), student.getGender());
	}



	@Test
  public void testMapStudentEntityToStudentNegative() {
    // Given
    StudentEntity studentEntity = null;


    // When
    Student student = studentServiceMapper.mapStudentEntityToStudent(studentEntity);

    // Then
    assertNull(student);

  }

	/**
	 * Test : Mapping from 'Student' to 'StudentEntity'
	 */
	@Test
	public void testMapStudentToStudentEntity() {
		// Given
		Student student = new Student();
		student.setFname(mockValues.nextString(50));
		student.setLname(mockValues.nextString(50));
		student.setAge(mockValues.nextBigDecimal());
		student.setGender(mockValues.nextString(2));

		StudentEntity studentEntity = new StudentEntity();

		// When
		studentServiceMapper.mapStudentToStudentEntity(student, studentEntity);

		// Then
		assertEquals(student.getFname(), studentEntity.getFname());
		assertEquals(student.getLname(), studentEntity.getLname());
		assertEquals(student.getAge(), studentEntity.getAge());
		assertEquals(student.getGender(), studentEntity.getGender());
	}

	@Test
  public void testMapStudentToStudentEntityNegative() {
    // Given
    Student student = null;

    StudentEntity studentEntity = null;

    // When
    studentServiceMapper.mapStudentToStudentEntity(student, studentEntity);

    // Then
    assertNull(studentEntity);
  }

}