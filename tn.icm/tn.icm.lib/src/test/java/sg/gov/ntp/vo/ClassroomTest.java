/*
 * Created on 7 Sep 2017 ( Time 14:00:22 )
 */
package sg.gov.ntp.vo;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import sg.gov.ntp.test.MockValues;


import java.math.BigDecimal;

public class ClassroomTest {

    private static Classroom classroomTestValue = null;

    private static MockValues mockValues = new MockValues();

    private static BigDecimal classroomIdInternalMockValue = mockValues.nextBigDecimal();
    private static BigDecimal classroomNoInternalMockValue = mockValues.nextBigDecimal();
    private static BigDecimal studentIdInternalMockValue = mockValues.nextBigDecimal();

    @Before
    public void setUp() throws Exception {

    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        classroomTestValue = new Classroom();
        
        // Prepare test data for Getter methods.
        classroomTestValue.setClassroomId(classroomIdInternalMockValue);
        classroomTestValue.setClassroomNo(classroomNoInternalMockValue);
        classroomTestValue.setStudentId(studentIdInternalMockValue);
    }

    @After
    public void tearDown() throws Exception {
    
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        classroomTestValue = null;
        mockValues = null;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    @Test
    public void testGetClassroomId () {
        assertEquals(classroomIdInternalMockValue, classroomTestValue.getClassroomId());
    }

    @Test
    public void testSetClassroomId () {
        classroomTestValue.setClassroomId(classroomIdInternalMockValue);
        assertEquals(classroomIdInternalMockValue, classroomTestValue.getClassroomId());
    }

    @Test
    public void testGetClassroomNo () {
        assertEquals(classroomNoInternalMockValue, classroomTestValue.getClassroomNo());
    }

    @Test
    public void testSetClassroomNo () {
        classroomTestValue.setClassroomNo(classroomNoInternalMockValue);
        assertEquals(classroomNoInternalMockValue, classroomTestValue.getClassroomNo());
    }

    @Test
    public void testGetStudentId () {
        assertEquals(studentIdInternalMockValue, classroomTestValue.getStudentId());
    }

    @Test
    public void testSetStudentId () {
        classroomTestValue.setStudentId(studentIdInternalMockValue);
        assertEquals(studentIdInternalMockValue, classroomTestValue.getStudentId());
    }


}
