package sg.gov.ntp.test;

import sg.gov.ntp.vo.Student;
import java.math.BigDecimal;

import org.junit.Test;

public class StudentFactoryForTest {

	private MockValues mockValues = new MockValues();

	public Student newStudent() {

		BigDecimal studentId = mockValues.nextBigDecimal();

		Student student = new Student();
		student.setStudentId(studentId);
		return student;
	}

	@Test
	public void StudentFactoryForTest() {

	}
}
