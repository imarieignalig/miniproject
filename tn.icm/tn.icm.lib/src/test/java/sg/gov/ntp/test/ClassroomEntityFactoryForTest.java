package sg.gov.ntp.test;

import sg.gov.ntp.vo.jpa.ClassroomEntity;
import java.math.BigDecimal;

import org.junit.Test;

public class ClassroomEntityFactoryForTest {

	private MockValues mockValues = new MockValues();

	public ClassroomEntity newClassroomEntity() {

		BigDecimal classroomId = mockValues.nextBigDecimal();

		ClassroomEntity classroomEntity = new ClassroomEntity();
		classroomEntity.setClassroomId(classroomId);
		return classroomEntity;
	}

	@Test
	public void ClassroomEntityFactoryForTest() {

	}
}
