package sg.gov.ntp.test;

import sg.gov.ntp.vo.jpa.StudentEntity;
import java.math.BigDecimal;

import org.junit.Test;

public class StudentEntityFactoryForTest {

	private MockValues mockValues = new MockValues();

	public StudentEntity newStudentEntity() {

		BigDecimal studentId = mockValues.nextBigDecimal();

		StudentEntity studentEntity = new StudentEntity();
		studentEntity.setStudentId(studentId);
		return studentEntity;
	}

	@Test
	public void StudentEntityFactoryForTest() {

	}
}
