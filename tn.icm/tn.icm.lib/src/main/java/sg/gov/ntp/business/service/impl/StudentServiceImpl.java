/*
 * Created on 7 Sep 2017 ( Time 14:00:01 )
 */
package sg.gov.ntp.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import sg.gov.ntp.vo.Student;
import sg.gov.ntp.vo.jpa.StudentEntity;
import java.math.BigDecimal;
import sg.gov.ntp.business.service.StudentService;
import sg.gov.ntp.business.service.mapping.StudentServiceMapper;
import sg.gov.ntp.data.repository.jpa.StudentJpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of StudentService
 */
@Component
@Transactional
public class StudentServiceImpl implements StudentService {

	@Resource
	private StudentJpaRepository studentJpaRepository;

	@Resource
	private StudentServiceMapper studentServiceMapper;

	//@Override
	public Student findById(BigDecimal studentId) {
		StudentEntity studentEntity = studentJpaRepository.findOne(studentId);
		return studentServiceMapper.mapStudentEntityToStudent(studentEntity);
	}

	//@Override
	public List<Student> findAll() {
		Iterable<StudentEntity> entities = studentJpaRepository.findAll();
		List<Student> beans = new ArrayList<Student>();
		for(StudentEntity studentEntity : entities) {
			beans.add(studentServiceMapper.mapStudentEntityToStudent(studentEntity));
		}
		return beans;
	}

	//@Override
	public Student save(Student student) {
		return update(student) ;
	}

	//@Override
	public Student create(Student student) {
		StudentEntity studentEntity = studentJpaRepository.findOne(student.getStudentId());
		if( studentEntity != null ) {
			throw new IllegalStateException("already.exists");
		}
		studentEntity = new StudentEntity();
		studentServiceMapper.mapStudentToStudentEntity(student, studentEntity);
		StudentEntity studentEntitySaved = studentJpaRepository.save(studentEntity);
		return studentServiceMapper.mapStudentEntityToStudent(studentEntitySaved);
	}

	//@Override
	public Student update(Student student) {
		StudentEntity studentEntity = studentJpaRepository.findOne(student.getStudentId());
		studentServiceMapper.mapStudentToStudentEntity(student, studentEntity);
		StudentEntity studentEntitySaved = studentJpaRepository.save(studentEntity);
		return studentServiceMapper.mapStudentEntityToStudent(studentEntitySaved);
	}

	//@Override
	public void delete(BigDecimal studentId) {
		studentJpaRepository.delete(studentId);
	}

	public StudentJpaRepository getStudentJpaRepository() {
		return studentJpaRepository;
	}

	public void setStudentJpaRepository(StudentJpaRepository studentJpaRepository) {
		this.studentJpaRepository = studentJpaRepository;
	}

	public StudentServiceMapper getStudentServiceMapper() {
		return studentServiceMapper;
	}

	public void setStudentServiceMapper(StudentServiceMapper studentServiceMapper) {
		this.studentServiceMapper = studentServiceMapper;
	}

  public void saveStudent(Student student) {
    this.save(student);
  }

}
