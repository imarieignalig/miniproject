/*
 * Created on 7 Sep 2017 ( Time 14:00:22 )
 */
package sg.gov.ntp.business.service;

import java.util.List;
import java.math.BigDecimal;

import sg.gov.ntp.vo.Classroom;

/**
 * Business Service Interface for entity Classroom.
 */
public interface ClassroomService {

	/**
	 * Loads an entity from the database using its Primary Key
	 * @param classroomId
	 * @return entity
	 */
	Classroom findById( BigDecimal classroomId  ) ;

	/**
	 * Loads all entities.
	 * @return all entities
	 */
	List<Classroom> findAll();

	/**
	 * Saves the given entity in the database (create or update)
	 * @param entity
	 * @return entity
	 */
	Classroom save(Classroom entity);

	/**
	 * Updates the given entity in the database
	 * @param entity
	 * @return
	 */
	Classroom update(Classroom entity);

	/**
	 * Creates the given entity in the database
	 * @param entity
	 * @return
	 */
	Classroom create(Classroom entity);

	/**
	 * Deletes an entity using its Primary Key
	 * @param classroomId
	 */
	void delete( BigDecimal classroomId );


}
