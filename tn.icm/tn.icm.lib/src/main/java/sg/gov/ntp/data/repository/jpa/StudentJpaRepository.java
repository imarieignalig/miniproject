package sg.gov.ntp.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import java.math.BigDecimal;
import sg.gov.ntp.vo.jpa.StudentEntity;

/**
 * Repository : Student.
 */
public interface StudentJpaRepository extends PagingAndSortingRepository<StudentEntity, BigDecimal> {

}
