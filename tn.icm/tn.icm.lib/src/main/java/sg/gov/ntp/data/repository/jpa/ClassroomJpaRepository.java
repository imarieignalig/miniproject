package sg.gov.ntp.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import java.math.BigDecimal;
import sg.gov.ntp.vo.jpa.ClassroomEntity;

/**
 * Repository : Classroom.
 */
public interface ClassroomJpaRepository extends PagingAndSortingRepository<ClassroomEntity, BigDecimal> {

}
