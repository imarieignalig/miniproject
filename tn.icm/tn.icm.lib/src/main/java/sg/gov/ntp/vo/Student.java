/*
 * Created on 7 Sep 2017 ( Time 14:00:01 )
 */
package sg.gov.ntp.vo;

import java.io.Serializable;

import javax.validation.constraints.*;

import java.math.BigDecimal;

public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @NotNull
    private BigDecimal studentId;

    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS
    //----------------------------------------------------------------------
    @NotNull
    @Size( min = 1, max = 50 )
    private String fname;

    @NotNull
    @Size( min = 1, max = 50 )
    private String lname;

    @NotNull
    private BigDecimal age;

    @NotNull
    @Size( min = 1, max = 2 )
    private String gender;



    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setStudentId( BigDecimal studentId ) {
        this.studentId = studentId ;
    }

    public BigDecimal getStudentId() {
        return this.studentId;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    public void setFname( String fname ) {
        this.fname = fname;
    }
    public String getFname() {
        return this.fname;
    }

    public void setLname( String lname ) {
        this.lname = lname;
    }
    public String getLname() {
        return this.lname;
    }

    public void setAge( BigDecimal age ) {
        this.age = age;
    }
    public BigDecimal getAge() {
        return this.age;
    }

    public void setGender( String gender ) {
        this.gender = gender;
    }
    public String getGender() {
        return this.gender;
    }
}
