/*
 * Created on 7 Sep 2017 ( Time 14:00:01 )
 */
// This Bean has a basic Primary Key (not composite)

package sg.gov.ntp.vo.jpa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Persistent class for entity stored in table "STUDENT"
 *
  *
 */

@Entity
@Table(name="STUDENT", schema="SYSTEM" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="StudentEntity.countAll", query="SELECT COUNT(x) FROM StudentEntity x" )
} )
@JsonInclude(Include.NON_NULL)
public class StudentEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @Id
    @Column(name="STUDENT_ID", nullable=false)
    @JsonProperty("studentId")
    private BigDecimal studentIdEntity    ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS
    //----------------------------------------------------------------------
    @Column(name="FNAME", nullable=false, length=50)
    @JsonProperty("fname")
    private String fnameEntity;

    @Column(name="LNAME", nullable=false, length=50)
    @JsonProperty("lname")
    private String lnameEntity;

    @Column(name="AGE", nullable=false)
    @JsonProperty("age")
    private BigDecimal ageEntity;

    @Column(name="GENDER", nullable=false, length=2)
    @JsonProperty("gender")
    private String genderEntity;



    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------
    @OneToMany(mappedBy="studentfk", targetEntity=ClassroomEntity.class)
    private List<ClassroomEntity> listOfClassroomfk;


    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public StudentEntity() {
		super();
    }

    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setStudentId( BigDecimal studentId ) {
        this.studentIdEntity = studentId ;
    }
    public BigDecimal getStudentId() {
        return this.studentIdEntity;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : FNAME ( VARCHAR2 )
    public void setFname( String fname ) {
        this.fnameEntity = fname;
    }
    public String getFname() {
        return this.fnameEntity;
    }

    //--- DATABASE MAPPING : LNAME ( VARCHAR2 )
    public void setLname( String lname ) {
        this.lnameEntity = lname;
    }
    public String getLname() {
        return this.lnameEntity;
    }

    //--- DATABASE MAPPING : AGE ( NUMBER )
    public void setAge( BigDecimal age ) {
        this.ageEntity = age;
    }
    public BigDecimal getAge() {
        return this.ageEntity;
    }

    //--- DATABASE MAPPING : GENDER ( VARCHAR2 )
    public void setGender( String gender ) {
        this.genderEntity = gender;
    }
    public String getGender() {
        return this.genderEntity;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------
    public void setListOfClassroomfk( List<ClassroomEntity> listOfClassroomfk ) {
        this.listOfClassroomfk = listOfClassroomfk;
    }
    public List<ClassroomEntity> getListOfClassroomfk() {
        return this.listOfClassroomfk;
    }
}
